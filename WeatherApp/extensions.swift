//
//  extensions.swift
//  WeatherApp
//
//  Created by Artem Karmaz on 12/9/18.
//  Copyright © 2018 Artem Karmaz. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIView {
    func fadeIn(_ duration: TimeInterval? = 0.2, onCompletion: (() -> Void)? = nil) {
        self.alpha = 0
        self.isHidden = false
        UIView.animate(withDuration: duration!,
                       animations: { self.alpha = 1 },
                       completion: { (value: Bool) in
                        if let complete = onCompletion { complete() }
        }
        )
    }
    
    func fadeOut(_ duration: TimeInterval? = 0.2, onCompletion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration!,
                       animations: { self.alpha = 0 },
                       completion: { (value: Bool) in
                        self.isHidden = true
                        if let complete = onCompletion { complete() }
        }
        )
    }
    
}

extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 9.0
        //        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        animation.values = [-7.0, 7.0, -7.0, 7.0, -7.0, 7.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        } else {
            print("Internet Connection not Available!")
        }
        
        let urlString = "https://api.apixu.com/v1/current.json?key=c5d57f8a831c4dde8fd153715180512&q=\(searchBar.text?.replacingOccurrences(of: " ", with: "%20") ?? "_")"
        var errorHasOccured: Bool = false
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, responce, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            
            
            do {
                let weather = try JSONDecoder().decode(Welcome.self, from: data)
                print(weather)
                
                // first queue sync
                DispatchQueue.main.async {
                    if errorHasOccured {
                        
                    } else {
                        self.tempCLabel.text = "\(weather.current.tempC)\u{2103}"
                        self.tempFLabel.text = "\(weather.current.tempF)\u{2109}"
                        self.conditionLabel.text = weather.current.condition.text
                    }
                }
                
            } catch let error {
                print(error)
            }
            
            
            
            
            } .resume()
        
        
        
        
    }
}
