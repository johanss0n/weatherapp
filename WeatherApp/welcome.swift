//
//  welcome.swift
//  WeatherApp
//
//  Created by Artem Karmaz on 12/9/18.
//  Copyright © 2018 Artem Karmaz. All rights reserved.
//

import Foundation

class Welcome: Codable {
    let location: Location
    let current: Current
    
    init(location: Location, current: Current) {
        self.location = location
        self.current = current
    }
}
